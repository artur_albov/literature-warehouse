<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="books.templ">
    <tiles:putAttribute name="content">

        <h2>Edit your book!</h2>
        <hr/>
        <div id="success_block">
        </div>


        <label class="col-sm-2 control-label">Title:</label>
        <div class="col-sm-10">
            <input type="text" id="inputTitle" class="form-control" value="${description.book.title}">
        </div>

        <br/><br/><br/>

        <label class="col-sm-2 control-label">Description:</label>
        <div class="col-sm-10">
            <input type="text" id="inputDescription" class="form-control" value="${description.book.description}">
        </div>

        <br/><br/><br/>

        <div class="col-sm-2">
            <b>Tags:</b>
        </div>
        <div class="col-sm-10">
            <div id="tags_block">
                <c:forEach items="${description.tags}" var="booktag">
                    <span class="label label-default" id="tag-${booktag.id}">${booktag.tag.name}</span>
                    <input type="image" src="/resources/images/icons/remove-icon.png" width="15px" height="15px"
                           id="deltag-${booktag.id}" onclick="deleteTag(event)">
                </c:forEach>
            </div>
        </div>


        <br/><br/>

        <div class="col-sm-2">
        </div>
        <div class="col-sm-2">
            <input id="addTagButton" type="button" class="btn btn-primary" value="Add Tag"/><br/>
        </div>
        <div id="tag_form">
            <div class="col-sm-5">
                <input id="tag_value" class="form-control" type="text" placeholder="Tag Name"/>
            </div>
            <div class="col-sm-3">
                <input id="submit_tag" class="btn btn-primary" type="button" value="Add"/>
                <input id="cancel_tag" class="btn btn-primary" type="button" value="Cancel"/>
            </div>
        </div>
        <br/>

        <div class="col-sm-12"></div> <br/><br/>

        <label class="col-sm-2 control-label">Genre:</label>
        <div class="col-sm-10">
            <select id="selectGenre" path="genreId">
                <option value="${description.book.genre.id}">${description.book.genre.name}</option>
                <c:forEach items="${genres}" var="genre">
                    <c:if test="${genre.id ne book.genre.id}">
                        <option value="${genre.id}">${genre.name}</option>
                    </c:if>
                </c:forEach>
            </select>
        </div>

        <br/>
            <input id="bookId" type="hidden" value="${description.book.id}"/>
            <input id="bookVersion" type="hidden" value="${description.book.version}"/>
            <input id="currIndex" type="hidden" value="${chaptersCount}"/>
        <br/>

        <div id="chapter_form">
            <hr/>
                <h2>New chapter</h2>
                <input id="chapter_name" class="form-control" type="text" placeholder="Title"/><br/>
                <textarea id="chapter_content" data-provide="markdown" data-iconlibrary="fa" rows="10"></textarea>
                <hr/>
                <input type="button" id="submit_chapter" class="btn btn-primary" value="Add"/>
                <input type="button" id="cancel_chapter" class="btn btn-primary" value="Cancel"/>
        </div>

        <div id="chapter_edit_form">
            <hr/>
            <h2>Edit chapter</h2>
            <input type="hidden" id="chapter_edit_version"/>
            <input type="hidden" id="chapter_edit_id"/>
            <input id="chapter_edit_name" class="form-control" type="text" placeholder="Title"/><br/>
            <textarea id="chapter_edit_content" data-provide="markdown" data-iconlibrary="fa" rows="10"></textarea>
            <hr/>
            <input type="button" id="submit_edit_chapter" class="btn btn-primary" value="Save"/>
            <input type="button" onclick="deleteChapter()" class="btn btn-primary" value="Delete"/>
            <input type="button" id="cancel_edit_chapter" class="btn btn-primary" value="Cancel"/>
        </div>

    </tiles:putAttribute>

    <tiles:putAttribute name="right-top-block">
        <div class="list-group" id="chapter_block">
            <a href="#" class="list-group-item disabled">
                <b>Chapters:</b>
            </a>

            <div id="sortableBlock">
                <c:forEach items="${description.chapters}" var="chapter">
                    <a onclick="editChapter(event)" id="chapter-${chapter.id}"
                       class="list-group-item">${chapter.name}</a>
                </c:forEach>
            </div>

        </div>
        <input type="button" class="btn btn-primary btn-block" id="add_chapter" value="Add new chapter"/>
        <input type="button" class="btn btn-primary btn-block" id="saveBookButton" value="Save Book"/><br/>
        <a href="/books/delete/${description.book.id}"><input type="button" class="btn btn-danger btn-block" value="Delete Book" id="deleteBookButton"/></a>
    </tiles:putAttribute>


</tiles:insertDefinition>
