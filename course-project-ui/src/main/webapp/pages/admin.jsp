<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>


<tiles:insertDefinition name="admin.templ">
    <tiles:putAttribute name="content">
        <table class="table table-striped" border="0">
            <thead>
            <tr>
                <th>Email</th>
                <th>Nickname</th>
                <th>Name</th>
                <th>Surname</th>
                <th>Edit</th>
                <th>Status</th>
            </tr>
            </thead>
            <c:forEach items="${users}" var="user">
                <tr>

                    <td>${user.email}</td>
                    <td>${user.nickname}</td>
                    <td>${user.name}</td>
                    <td>${user.surname}</td>
                    <td>
                        <a class="btn btn-danger" href="/admin/panel/delete/${user.id}">delete</a>
                        <c:choose>
                            <c:when test="${user.banned}">
                                <a class="btn btn-primary" href="/admin/panel/unban/${user.id}">unban</a>
                            </c:when>
                            <c:otherwise>
                                <a class="btn btn-primary" href="/admin/panel/ban/${user.id}">ban</a>
                            </c:otherwise>
                        </c:choose>
                        <a class="btn btn-primary" href="/admin/panel/reset/${user.id}">reset password</a>
                    </td>
                    <td>
                        <c:choose>
                            <c:when test="${user.banned}">
                                <strong>Banned</strong>
                            </c:when>
                            <c:otherwise>
                                <strong>Active</strong>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </c:forEach>
        </table>

        <hr/>
        <div style="text-align: center">Designed by Artur Albov</div>
    </tiles:putAttribute>
</tiles:insertDefinition>

