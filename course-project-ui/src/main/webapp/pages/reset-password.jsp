<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="home.templ">
    <tiles:putAttribute name="content">
        <h1 class="cover-heading">New Password</h1>
        <p><small>Your password was reseted! Please enter new password.</small></p>
        <div style="width: 400px; margin: auto;">
            <form:form class="col-md-12" method="post" modelAttribute="pass_form">
                <div class="form-group">
                    <p style="color: red;"><form:errors path="oldPassword"/></p>
                    <form:input path="oldPassword" type="password" class="form-control" style="height: 30px; width: 100%;" placeholder="Old Password"/>
                </div>
                <div class="form-group">
                    <p style="color: red;"><form:errors path="newPassword"/></p>
                    <form:input path="newPassword" type="password" class="form-control" style="height: 30px; width: 100%;" placeholder="New Password"/>
                </div>
                <div class="form-group">
                    <p style="color: red;"><form:errors path="confirmNewPassword"/></p>
                    <form:input path="confirmNewPassword" type="password" class="form-control" style="height: 30px; width: 100%;" placeholder="Confirm New Password"/>
                </div>
                <div class="form-group">
                    <input type="submit" value="Send" class="btn btn-primary btn-lg btn-block"/>
                </div>
            </form:form>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>