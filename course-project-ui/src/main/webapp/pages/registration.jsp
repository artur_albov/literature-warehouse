<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<s:message code="input.placeholder.email" var="email"/>
<s:message code="input.placeholder.password" var="password"/>
<s:message code="input.placeholder.cofirmPassword" var="confirmPassword"/>
<s:message code="input.placeholder.name" var="name"/>
<s:message code="input.placeholder.surname" var="surname"/>
<s:message code="input.placeholder.nickname" var="nickname"/>
<s:message code="input.button.registration" var="register"/>
<s:message code="registration.title" var="title"/>
<s:message code="registration.email.confirm" var="emailConfirm"/>

<tiles:insertDefinition name="home.templ">
    <tiles:putAttribute name="content">
        <h1 class="cover-heading">${title}</h1>
        <p><small>${emailConfirm}</small></p>
        <div style="width: 400px; margin: auto;">
            <form:form class="col-md-12" method="post" modelAttribute="reg_form">
                <div class="form-group">
                    <p style="color: red;"><form:errors path="email"/><br/></p>
                    <form:input path="email" type="text" class="form-control" style="height: 30px; width: 100%;" placeholder="${email}"/>
                </div>
                <div class="form-group">
                    <p style="color: red;"><form:errors path="password"/></p>
                    <form:input path="password" type="password" class="form-control" style="height: 30px; width: 100%;" placeholder="${password}"/>
                </div>
                <div class="form-group">
                    <p style="color: red;"><form:errors path="confirmPassword"/></p>
                    <form:input path="confirmPassword" type="password" class="form-control" style="height: 30px; width: 100%;" placeholder="${confirmPassword}"/>
                </div>
                <div class="form-group">
                    <p style="color: red;"><form:errors path="name"/></p>
                    <form:input path="name" type="text" class="form-control" style="height: 30px; width: 100%;" placeholder="${name}"/>
                </div>
                <div class="form-group">
                    <p style="color: red;"><form:errors path="surname"/></p>
                    <form:input path="surname" type="text" class="form-control" style="height: 30px; width: 100%;" placeholder="${surname}"/>
                </div>
                <div class="form-group">
                    <p style="color: red;"><form:errors path="nickname"/></p>
                    <form:input path="nickname" type="text" class="form-control" style="height: 30px; width: 100%;" placeholder="${nickname}"/>
                </div>
                <div class="form-group">
                    <input type="submit" value="${register}" class="btn btn-primary btn-lg btn-block"/>
                </div>
            </form:form>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>