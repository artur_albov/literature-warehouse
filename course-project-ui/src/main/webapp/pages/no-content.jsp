<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<tiles:insertDefinition name="reader.templ">
    <tiles:putAttribute name="header">
        <div class="pull-right">
            <br/>
            <a href="/">Home</a> | <a href="/lib/0">Books</a> <sec:authorize ifAnyGranted="ROLE_USER,ROLE_ADMIN">| <a href="/books/my">My Books</a></sec:authorize>
            <br/>
        </div>
    </tiles:putAttribute>

    <tiles:putAttribute name="content">
       <h1>Sorry, this book have no content :(</h1>
    </tiles:putAttribute>

    <tiles:putAttribute name="footer">
        &nbsp;
    </tiles:putAttribute>

</tiles:insertDefinition>


