<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<tiles:insertDefinition name="home.templ">
    <tiles:putAttribute name="content">
        <h1 class="cover-heading">Confirmation</h1>
        <p class="lead">We've send you confirmation letter on your email. Please, follow the link in the letter to activate your account.</p>
    </tiles:putAttribute>
</tiles:insertDefinition>