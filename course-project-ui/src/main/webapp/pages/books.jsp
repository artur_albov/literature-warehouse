<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="th" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<tiles:insertDefinition name="books.templ">
    <tiles:putAttribute name="content">
        <!--/stories-->

        <h2><div style="padding-left: 60px">Latest Books</div> </h2><br/>

        <c:forEach items="${page.content}" var="book">
            <div class="row">
                <br>
                <div class="col-md-2 col-sm-3 text-center">
                    <a class="story-title" href="#"><img alt="" src="http://www.radionetplus.ru/uploads/posts/2013-03/1363237314_oboi-knigi-8.jpg" style="width:100px;height:100px" class="img-circle"></a>
                </div>
                <div class="col-md-10 col-sm-9">
                    <h3>
                        <a href="/show/${book.id}">${book.title}</a>&nbsp;
                        <sec:authorize access="hasRole('ROLE_ADMIN') or (isAuthenticated() and principal.user.id==${book.user.id})">
                            <a href="/books/edit/${book.id}"><input type="image"
                                                                                src="/resources/images/icons/edit-icon.png"
                                                                                width="20px" height="20px"/></a>
                            <a href="/books/delete/${book.id}"><input type="image"
                                                                      src="/resources/images/icons/delete-icon.png"
                                                                      width="20px" height="20px"/></a>
                        </sec:authorize>
                    </h3>
                    <small>${book.description}</small>
                    <div class="row">
                        <div class="col-xs-9">
                            <h4><span class="label label-default">by ${book.user.name} ${book.user.surname}</span></h4><h4>
                            <br/>
                            <small style="font-family:courier,'new courier';" class="text-muted">Last modified: ${book.lastModified}</small>
                        </h4></div>
                    </div>
                    <br><br>
                </div>
            </div>
            <hr/>
        </c:forEach>
        <c:if test="${page.first ne true}">
            <a class="pull-left" href="/lib/${page.number-1}"><b><-Back</b></a>&nbsp;
        </c:if>
        <c:if test="${page.last ne true}">
            <a class="pull-right" href="/lib/${page.number+1}"><b>Forward-></b></a>
        </c:if>
        <!--/stories-->

    </tiles:putAttribute>

    <tiles:putAttribute name="right-top-block">
        <h2>Top Books</h2><br/>
        <c:forEach items="${topBooks}" var="book">
            <h4><a href="/show/${book.id}">${book.title}</a></h4>
            <small>by ${book.user.name} ${book.user.surname}</small>
            <hr/>
        </c:forEach>
    </tiles:putAttribute>

</tiles:insertDefinition>