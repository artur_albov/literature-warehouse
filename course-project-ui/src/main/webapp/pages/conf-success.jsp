<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<tiles:insertDefinition name="home.templ">
    <tiles:putAttribute name="content">
        <h1 class="cover-heading">Congratulations, ${user.name}!</h1>
        <p class="lead">Now your account is <text-success>active</text-success>! Sign in and become the best writter!</p>
    </tiles:putAttribute>
</tiles:insertDefinition>
