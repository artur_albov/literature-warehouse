<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<s:message code="home.message.about" var="aboutUs"/>
<s:message code="home.message.enter" var="enterButton"/>
<s:message code="home.message.newuser" var="firstTime"/>
<s:message code="home.message.readmode" var="readMode"/>
<s:message code="home.message.signup" var="signUp"/>
<s:message code="home.title" var="title"/>
<s:message code="home.message.free" var="free"/>
<s:message code="input.button.signin" var="signin"/>
<s:message code="input.placeholder.email" var="email"/>
<s:message code="input.placeholder.password" var="password"/>

<tiles:insertDefinition name="home.templ">
    <tiles:putAttribute name="content">
        <h1 class="cover-heading">${title}</h1>
        <p class="lead">${aboutUs}</p>
        <sec:authorize ifNotGranted="ROLE_USER,ROLE_ADMIN">
            <div style="width: 250px; margin: auto;">
                <p class="first_time_text">
                    <small>${firstTime} <a href="/registration">${signUp}</a> ${free}</small>
                </p>
                <p style="color: red;">${errorMessage}</p>
                <form class="col-md-12" action='<c:url value="/j_spring_security_check"></c:url>' method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" name="j_username" style="height: 30px; width: 100%;"
                               placeholder="${email}">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="j_password" style="height: 30px; width: 100%;"
                               placeholder="${password}">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="${signin}" class="btn btn-primary btn-lg btn-block"/>
                    </div>
                </form>
            </div>
            <p>${readMode}</p>
        </sec:authorize>
        <p class="lead">
            <a href="/lib/0" class="btn btn-lg btn-default">${enterButton}</a>
        </p>
    </tiles:putAttribute>
</tiles:insertDefinition>