<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>

<s:message code="menu.home" var="home"/>
<s:message code="menu.mybooks" var="myBooks"/>
<s:message code="menu.books" var="booksPage"/>
<s:message code="dark" var="dark"/>
<s:message code="light" var="light"/>
<s:message code="menu.theme" var="menuTheme"/>

<tiles:insertDefinition name="reader.templ">
   <tiles:putAttribute name="header">
       <div class="pull-right">
           <a href="/">${home}</a> | <a href="/lib/0">${booksPage}</a> <sec:authorize ifAnyGranted="ROLE_USER,ROLE_ADMIN">| <a href="/books/my">${myBooks}</a></sec:authorize>
       </div>
       <h1>
               ${description.book.title}&nbsp;
                   <sec:authorize access="(isAuthenticated() and principal.user.id==${description.book.user.id}) or hasRole('ROLE_ADMIN')">
                   <a href="/books/edit/${description.book.id}"
                      title="Edit book"><input type="image" width="30px" height="30px"
                                               src="/resources/images/icons/edit-icon.png"/></a>
                   </sec:authorize>
       </h1>
       <input type="hidden" value="${chapter.version}" id="chapterVersion"/>

       <div class="row">
           <div class="col-md-8">
               <div class="reader-header-desc">
                   &nbsp;&nbsp;&nbsp;&nbsp;<b>Author:</b> ${description.book.user.name} ${description.book.user.surname}<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;<b>Genre:</b> ${description.book.genre.name} <br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;<b>Description:</b> ${description.book.description} <br/>
                   <br/>
                   <%--Content: <br/>--%>
                   <%--<c:forEach items="${description.chapters}" var="chap">--%>
                       <%--<a href="/show/${description.book.id}/${chap.index}">Chapter ${chap.index}: ${chap.name}</a><br/>--%>
                   <%--</c:forEach>--%>
               </div>
           </div>
           <div class="col-md-4">
               <sec:authorize ifAnyGranted="ROLE_USER,ROLE_ADMIN">
                   <div class="reader-rate">
                       <small>Like it? Rate this book!</small>
                       <div id="rating">
                           <input type="hidden" name="val" value="${description.rating}"/>
                           <input type="hidden" name="votes" value="${description.votes}"/>
                       </div>
                   </div>
               </sec:authorize>
           </div>
       </div>
   </tiles:putAttribute>

    <tiles:putAttribute name="content">
        <h2>Chapter ${chapter.index}: ${chapter.name}</h2> <br/>
        <input type="hidden" value="${chapter.id}" id="chapterId"/>



        <div id="reader-content">
            <c:forEach items="${parts}" var="part" varStatus="loop">

                <sec:authorize ifAnyGranted="ROLE_ADMIN,ROLE_USER">
                    <div class="annotation" id="a-${loop.index}">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="pull-right"><input type="image" src="/resources/images/icons/close-icon.png"
                                                               width="20px" height="20px"
                                                               onclick="closeAnnotation(event)"
                                                               id="close-${loop.index}"/></div>
                                <br/>

                                <div id="content-${loop.index}">
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control col-sm-6" placeholder="Text"
                                               id="t-${loop.index}"/>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="button" class="btn btn-primary" value="Add" id="add-${loop.index}"
                                               onclick="addAnnotation(event)"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </sec:authorize>
                <div class="row">
                    <sec:authorize ifAnyGranted="ROLE_ADMIN,ROLE_USER">
                        <div class="col-sm-1">
                            <input type="image" src="/resources/images/icons/note.png" data-toggle="popover"
                                   width="25px" height="25px" id="show-${loop.index}" onclick="showAnnotation(event)"/>
                        </div>
                    </sec:authorize>
                    <div class="col-sm-11">
                        <p id="${loop.index}">
                                ${part}
                        </p>
                    </div>
                </div>
            </c:forEach>
        </div>
    </tiles:putAttribute>

    <tiles:putAttribute name="footer">
        Size:
        <input type="button" id="text_small" class="btn btn-default" value="small"/>
        <input type="button" id="text_middle" class="btn btn-default" value="middle"/>
        <input type="button" id="text_large" class="btn btn-default" value="large"/>
        Widht:
        <input type="button" id="width_small" class="btn btn-default" value="small"/>
        <input type="button" id="width_middle" class="btn btn-default" value="middle"/>
        <input type="button" id="width_large" class="btn btn-default" value="large"/>
        <sec:authorize ifAnyGranted="ROLE_ADMIN,ROLE_USER">
            <input type="button" id="annotation_block" class="btn btn-default" value="Annotations mode"/>
        </sec:authorize>
        <div class="pull-right">
            <c:if test="${chapter.index ne 0}">
                <a href="/show/${chapter.bookId}/${chapter.index-1}"><-Back</a>
            </c:if>
            <c:if test="${chapter.index ne chaptersSize-1}">
                <a href="/show/${chapter.bookId}/${chapter.index+1}">Forward-></a>
            </c:if>
        </div>
    </tiles:putAttribute>

</tiles:insertDefinition>


