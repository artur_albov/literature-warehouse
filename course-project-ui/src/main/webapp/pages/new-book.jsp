<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="books.templ">
    <tiles:putAttribute name="content">

        <h2>Add Your New Book!</h2>

        <form:form modelAttribute="book_form" method="post">
            <div style="color: red"><form:errors path="title"/></div>
            <form:input path="title" class="form-control" type="text" placeholder="Title"/><br/>
            <div style="color: red"><form:errors path="description"/></div>
            <form:input path="description" placeholder="Description" class="form-control" type="text"/><br/>
            Choose category:
            <form:select path="genreId" id="selectGenre">
                <c:forEach items="${genres}" var="genre">
                    <form:option value="${genre.id}">${genre.name}</form:option>
                </c:forEach>
            </form:select>
            <hr/>
            <input value="Add" type="submit" class="btn btn-primary"/>
        </form:form>

    </tiles:putAttribute>

    <tiles:putAttribute name="right-top-block">
        <h2>Top Books</h2><br/>
        <c:forEach items="${topBooks}" var="book">
            <h4><a href="/show/${book.id}">${book.title}</a></h4>
            <small>by ${book.user.name} ${book.user.surname}</small>
            <hr/>
        </c:forEach>
    </tiles:putAttribute>



</tiles:insertDefinition>

