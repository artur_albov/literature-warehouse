<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>

<s:message code="home.title" var="title"/>
<s:message code="header.desc" var="desc"/>
<s:message code="header.search" var="search"/>
<s:message code="header.search.desc" var="searchDescription"/>
<s:message code="header.search.placeholder" var="searchPlaceholder"/>


<h1>${title}</h1>
<p>${desc}</p>
<br/>
<p>${searchDescription}</p>
<div style="margin: auto; width:300px">
    <div class="row">
        <div class="col-sm-8">
            <input id="q" type="text" class="form-control" placeholder="${searchPlaceholder}"/>
        </div>
        <div class="col-sm-4">
            <input onclick="search(event)" class="btn btn-primary" type="button" value="${search}"/>
        </div>

    </div>
</div>
