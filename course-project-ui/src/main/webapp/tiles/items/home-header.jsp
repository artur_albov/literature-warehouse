<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>

<s:message code="menu.home" var="home"/>
<s:message code="home.title" var="title"/>
<s:message code="menu.mybooks" var="myBooks"/>
<s:message code="menu.books" var="books"/>
<s:message code="menu.logout" var="logout"/>

<h3 class="masthead-brand">${title}</h3>
<ul class="nav masthead-nav">
    <li class="active"><a href="/">${home}</a></li>
    <li><a href="/lib/0">${books}</a></li>
    <li><a href="/books/my">${myBooks}</a></li>
</ul>