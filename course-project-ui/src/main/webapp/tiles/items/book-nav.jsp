<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>

<s:message code="menu.home" var="home"/>
<s:message code="home.title" var="title"/>
<s:message code="menu.mybooks" var="myBooks"/>
<s:message code="menu.books" var="booksPage"/>
<s:message code="menu.logout" var="logout"/>
<s:message code="ru" var="ru"/>
<s:message code="en" var="en"/>
<s:message code="dark" var="dark"/>
<s:message code="light" var="light"/>
<s:message code="book.new" var="newBook"/>
<s:message code="menu.admin" var="adminPanel"/>
<s:message code="menu.lang" var="menuLanguage"/>
<s:message code="menu.theme" var="menuTheme"/>


<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">${title}</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li class="active"><a href="/">${home}</a></li>
            <li><a href="/lib/0">${booksPage}</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <sec:authorize access="isAuthenticated()">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><sec:authentication property="principal.user.email"/><span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="/books/new"><img src="/resources/images/icons/plus-icon.png" width="15px" height="15px"/>&nbsp;${newBook}</a></li>
                        <li><a href="/books/my"><img src="/resources/images/icons/book-icon.png" width="15px" height="15px"/>&nbsp;${myBooks}</a></li>
                        <li><a href="/logout"><img src="/resources/images/icons/exit-icon.png" width="15px" height="15px"/>&nbsp;${logout}</a></li>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li><a href="/admin/panel/"><img src="/resources/images/icons/user-icon.png" width="15px" height="15px"/>&nbsp;${adminPanel}</a></li>
                        </sec:authorize>
                    </ul>
                </li>
            </sec:authorize>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">${menuLanguage}<span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="${requestScope['javax.servlet.forward.request_uri']}?language=en">${en}</a></li>
                    <li><a href="${requestScope['javax.servlet.forward.request_uri']}?language=ru">${ru}</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">${menuTheme}<span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="${requestScope['javax.servlet.forward.request_uri']}?theme=dark">${dark}</a></li>
                    <li><a href="${requestScope['javax.servlet.forward.request_uri']}?theme=light">${light}</a></li>
                </ul>
            </li>
        </ul>
    </div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
