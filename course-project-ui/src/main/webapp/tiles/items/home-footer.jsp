<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>

<s:message code="footer.by" var="by"/>

<s:message code="ru" var="ru"/>
<s:message code="en" var="en"/>
<s:message code="dark" var="dark"/>
<s:message code="light" var="light"/>

<p>${by}</p>
<a href="${requestScope['javax.servlet.forward.request_uri']}?language=en"><small>${en}</small></a> | <a href="${requestScope['javax.servlet.forward.request_uri']}?language=ru"><small>${ru}</small></a><br/>
<a href="${requestScope['javax.servlet.forward.request_uri']}?theme=dark"><small>${dark}</small></a> | <a href="${requestScope['javax.servlet.forward.request_uri']}?theme=light"><small>${light}</small></a>