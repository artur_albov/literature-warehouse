<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html" charset="utf-8"/>
    <title>Reader</title>
    <c:set var="path"><s:theme code="styleSheet.reader"/></c:set>
    <link href="${path}" rel="stylesheet" type="text/css"/>
    <%--<link href="/resources/css/reader-dark-template.css" rel="stylesheet" type="text/css"/>--%>
    <link href="/resources/css/jquery.rating.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        window.jQuery || document.write('<script type="text/javascript" src="/resources/js/jquery-2.1.1.js"><\/script>');
    </script>
    <script type="text/javascript" src="/resources/js/jquery.rating-2.0.js"></script>
    <script type="text/javascript">
        $(function(){

            $('#rating').rating({
                fx: 'full',
                image: '/resources/images/stars.png',
                loader: '/resources/images/ajax-loader.gif',
                url: '${requestScope['javax.servlet.forward.request_uri']}/score',
                callback: function(responce){

                    this.vote_success.fadeOut(2000);
                }
            });
        })
    </script>
</head>
<body onload="init()">

<div class="container">
    <div class="col-md-12">

        <div id="reader-header">
            <tiles:insertAttribute name="header"/>
        </div>
        <hr/>
        <tiles:insertAttribute name="content"/>



    </div>
</div>




<div id="footer_fixed">
    <tiles:insertAttribute name="footer"/>
</div>

<!--Scripts-->
<script src="/resources/js/reader.js"></script>

</body>
</html>