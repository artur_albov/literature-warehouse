<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <meta http-equiv="content-type" content="text/html" charset="utf-8"/>
    <c:set var="path"><s:theme code="styleSheet.home"/></c:set>
    <link href="${path}" rel="stylesheet" type="text/css"/>
    <title>LiterWar</title>
</head>

<body>
<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">


            <div class="masthead clearfix">
                <div class="inner">
                    <tiles:insertAttribute name="header"/>
                </div>
            </div>

            <div class="inner cover">
                <tiles:insertAttribute name="content"/>
            </div>

            <div class="mastfoot">
                <div class="inner">
                    <tiles:insertAttribute name="footer"/>
                </div>
            </div>

        </div>

    </div>

</div>
</body>


</html>