
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>LiterWar</title>
    <c:set var="path"><s:theme code="styleSheet.book"/></c:set>
    <link href="${path}" rel="stylesheet" type="text/css"/>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="/resources/css/bootstrap-markdown.min.css" rel="stylesheet"/>

</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <tiles:insertAttribute name="navigation"/>
</nav>

<div class="top_header">
    <tiles:insertAttribute name="header"/>
</div>

<div style="padding-top: 50px;">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel">
                    <div class="panel-body">
                        <tiles:insertAttribute name="content"/>
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <div class="well">
                    <tiles:insertAttribute name="right-top-block"/>
                </div>
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="my_footer">
        <tiles:insertAttribute name="footer"/>
    </div>
</footer>


<!-- Scripts Include -->
<script src="//google-code-prettify.googlecode.com/svn/loader/run_prettify.js"type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/jquery-ui.min.js"></script>
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="/resources/js/markdown.js"></script>
<script src="/resources/js/to-markdown.js"></script>
<script src="/resources/js/bootstrap-markdown.js"></script>
<script src="//rawgit.com/jeresig/jquery.hotkeys/master/jquery.hotkeys.js"></script>
<script src="<c:url value="/resources/js/jquery.autocomplete.min.js" />"></script>
<script src="/resources/js/tagcloud.jquery.min.js"></script>
<script src="/resources/js/main.js"></script>
<script src="/resources/js/book.js"></script>

</body>
</html>
