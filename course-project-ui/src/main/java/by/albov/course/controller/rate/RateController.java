package by.albov.course.controller.rate;

import by.albov.course.entity.BookRate;
import by.albov.course.entity.common.Rate;
import by.albov.course.service.rate.RateService;
import by.albov.course.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by USER on 01.10.2014.
 */
@Controller
@RequestMapping("/show/{book_id}/{index}")
public class RateController {

    @Autowired
    private RateService rateService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/score",method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE) @ResponseBody
    public Map<String,String> addRate(@RequestBody RateRequest rateRequest,@PathVariable(value = "book_id")Long bookId){
        Map<String,String> result= new HashMap<String,String>();
        if(rateService.findRateByBookId(bookId)!=null){
            result.put("status","ERR");
            result.put("msg","You already vote for this book!");
            return result;
        }
        rateService.saveAndFlush(new BookRate(Rate.getOfferRateByScore(rateRequest.score),
                userService.getLoggedUserDetails().getUser().getId(),bookId));
        result.put("status", "OK");
        result.put("msg","Thank you, your vote is admitted!");
        return result;
    }


}
