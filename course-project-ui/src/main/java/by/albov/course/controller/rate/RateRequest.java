package by.albov.course.controller.rate;

/**
 * Created by USER on 01.10.2014.
 */
public class RateRequest {
    public double val;
    public int score;
    public int votes;

    public RateRequest() {
    }

    public RateRequest(double val, int score, int votes) {
        this.val = val;
        this.score = score;
        this.votes = votes;
    }

    public double getVal() {
        return val;
    }

    public void setVal(double val) {
        this.val = val;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }
}
