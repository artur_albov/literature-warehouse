package by.albov.course.controller.form;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * Created by Администратор on 23.09.2014.
 */
public class RegistrationForm {
    @Email(message = "Incorrect email")
    @NotEmpty(message = "Incorrect email")
    private String email;
    @Size(max = 15, min = 5,message = "Must be over 5 under 15 chars long")
    private String password;
    @Size(max = 15,min = 5,message = "Must be over 5 under 15 chars long")
    private String confirmPassword;
    @NotEmpty(message = "Should not be empty")
    private String name;
    @NotEmpty(message = "Should not be empty")
    private String surname;
    @NotEmpty(message = "Should not be empty")
    private String nickname;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
