package by.albov.course.controller;

import by.albov.course.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by USER on 09.10.2014.
 */
@Controller
@RequestMapping("/admin/panel")
public class AdminUserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "")
    public ModelAndView showAdminPanel(){
        ModelAndView mav = new ModelAndView();
        mav.addObject("users",userService.getAll());
        mav.setViewName("admin");
        return mav;
    }

    @RequestMapping(value = "/ban/{user_id}")
    public ModelAndView banUser(@PathVariable(value = "user_id")Long userId){
        ModelAndView mav = new ModelAndView();
        userService.banUser(userId);
        mav.setViewName("redirect:/admin/panel");
        return mav;
    }

    @RequestMapping(value = "/unban/{user_id}")
    public ModelAndView unbanUser(@PathVariable(value = "user_id")Long userId){
        ModelAndView mav = new ModelAndView();
        userService.unbanUser(userId);
        mav.setViewName("redirect:/admin/panel");
        return mav;
    }

    @RequestMapping(value = "/delete/{user_id}")
    public ModelAndView deleteUser(@PathVariable(value = "user_id")Long userId){
        ModelAndView mav = new ModelAndView();
        userService.delete(userService.getById(userId));
        mav.setViewName("redirect:/admin/panel");
        return mav;
    }

    @RequestMapping(value = "/reset/{user_id}")
    public ModelAndView resetUserPassword(@PathVariable(value = "user_id")Long userId){
        ModelAndView mav = new ModelAndView();
        userService.resetUserPassword(userId);
        mav.setViewName("redirect:/admin/panel");
        return mav;
    }
}
