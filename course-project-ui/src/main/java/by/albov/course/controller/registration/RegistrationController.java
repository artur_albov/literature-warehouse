package by.albov.course.controller.registration;

import by.albov.course.controller.form.RegistrationForm;
import by.albov.course.entity.User;
import by.albov.course.entity.common.UserRole;
import by.albov.course.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by Администратор on 23.09.2014.
 */
@Controller
@RequestMapping("/registration")
public class RegistrationController {

    @Autowired
    UserService userService;

    @Autowired
    private MailSender mailSender;

    @Autowired
    private SimpleMailMessage templateMessage;


    @RequestMapping(value = "",method = RequestMethod.GET)
    public ModelAndView showRegistrationPage(){
        ModelAndView mav = new ModelAndView();
        mav.addObject("reg_form",new RegistrationForm());
        mav.setViewName("registration");
        return mav;
    }

    @RequestMapping(value = "",method = RequestMethod.POST)
    public ModelAndView createNewUser(@Valid @ModelAttribute("reg_form") RegistrationForm registrationForm,BindingResult result){
        ModelAndView mav = new ModelAndView();
        if(!result.hasErrors()){
            if(registrationForm.getConfirmPassword().equals(registrationForm.getPassword())){
                try{
                    createNewUser(registrationForm);
                } catch (Exception e){
                    result.rejectValue("email", "error.user", "An account already exists for this email");
                    mav.setViewName("registration");
                    return mav;
                }
                mav.setViewName("redirect:/registration/confirmation");
                return mav;
            }  else {
                result.rejectValue("confirmPassword", "error.user","Passwords not match");
                result.rejectValue("password", "error.user","Passwords not match");
            }
        }
        mav.setViewName("registration");
        return mav;
    }



    public User createNewUser(RegistrationForm registrationForm){
        User createdUser = userService.saveAndFlush(new User(registrationForm.getEmail(), registrationForm.getPassword(),
                registrationForm.getName(), registrationForm.getSurname(),
                registrationForm.getNickname(), UserRole.ROLE_USER, false, false, false));
        sendConfirmationEmail(createdUser);
        userService.saveAndFlush(createdUser);
        return createdUser;
    }

    public void sendConfirmationEmail(User user){
        SecureRandom secureRandom = new SecureRandom();
        String code = new BigInteger(130,secureRandom).toString(32);
        user.setConfirmationToken(code);
        SimpleMailMessage msg = new SimpleMailMessage(templateMessage);
        msg.setTo(user.getEmail());
        msg.setText("Dear, "+user.getName() + " " + user.getSurname()
                +"!\nThank you for sign up in on our site!\n"
                +"Please follow this link to continue registration:\n"
                +"http://localhost:8080/registration/confirmation/success?confirm="+user.getConfirmationToken());
        try{
            this.mailSender.send(msg);
        }
        catch (MailException ex) {
            System.err.println(ex.getMessage());
        }
    }


}
