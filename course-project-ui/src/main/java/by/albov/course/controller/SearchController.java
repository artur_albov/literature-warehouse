package by.albov.course.controller;

import by.albov.course.entity.Book;
import by.albov.course.service.book.BookSearchService;
import by.albov.course.service.book.BookService;
import by.albov.course.service.rate.RateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by USER on 10.10.2014.
 */
@Controller
public class SearchController {

    @Autowired
    private BookSearchService bookSearchService;
    @Autowired
    private RateService rateService;

    @RequestMapping(value = "/search",method = RequestMethod.GET)
    public ModelAndView searchBook(@RequestParam(value = "q",required = false)String question,HttpServletRequest request){
        ModelAndView mav = new ModelAndView();
        if("".equals(question)){
            mav.setViewName("redirect:/lib/0");
            return mav;
        }
        mav.addObject("searchResults",bookSearchService.search(question));
        mav.addObject("topBooks",rateService.getTopBooks(5,0));
        mav.setViewName("search");
        return mav;
    }

    @RequestMapping(value = "/search/reindex",method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView reindexEntities(){
        ModelAndView mav = new ModelAndView();
        bookSearchService.reindex();
        mav.setViewName("redirect:/admin/panel");
        return mav;
    }

}
