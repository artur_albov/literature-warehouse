package by.albov.course.controller.chapter;

/**
 * Created by USER on 25.09.2014.
 */
public class ChapterRequest {
    private String name;
    private String content;
    private Long bookId;
    private Integer index;
    private Long bookVersion;

    public ChapterRequest() {
    }

    public ChapterRequest(String name, String content) {
        this.name = name;
        this.content = content;
    }

    public ChapterRequest(String name, String content, Long bookId) {
        this.name = name;
        this.content = content;
        this.bookId = bookId;
    }

    public ChapterRequest(String name, String content, Long bookId, Integer index) {
        this.name = name;
        this.content = content;
        this.bookId = bookId;
        this.index = index;
    }

    public ChapterRequest(String name, String content, Long bookId, Integer index, Long bookVersion) {
        this.name = name;
        this.content = content;
        this.bookId = bookId;
        this.index = index;
        this.bookVersion = bookVersion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Long getBookVersion() {
        return bookVersion;
    }

    public void setBookVersion(Long bookVersion) {
        this.bookVersion = bookVersion;
    }
}
