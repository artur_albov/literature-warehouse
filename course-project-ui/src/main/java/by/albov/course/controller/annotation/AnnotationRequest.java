package by.albov.course.controller.annotation;

/**
 * Created by USER on 04.10.2014.
 */
public class AnnotationRequest {
    private String content;

    public AnnotationRequest() {
    }

    public AnnotationRequest(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
