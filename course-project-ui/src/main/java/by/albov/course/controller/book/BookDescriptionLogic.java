package by.albov.course.controller.book;

import by.albov.course.entity.Book;
import by.albov.course.service.booktag.BookTagService;
import by.albov.course.service.chapter.ChapterService;
import by.albov.course.service.rate.RateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER on 03.10.2014.
 */
@Component
public class BookDescriptionLogic {
    @Autowired
    private RateService rateService;

    @Autowired
    private ChapterService chapterService;

    @Autowired
    private BookTagService bookTagService;

    public BookDescription getBookDescription(Book book) {
        BookDescription description = new BookDescription(book, rateService.getBookRate(book.getId()),
                rateService.getVotesCount(book.getId()), chapterService.getChapterByBookId(book.getId()),
                bookTagService.findByBookId(book.getId()));

        return description;
    }

    public List<BookDescription> getBookDescriptionList(List<Book> books){
        List<BookDescription> bookDescriptions = new ArrayList<BookDescription>();
        for (Book book: books){
            bookDescriptions.add(getBookDescription(book));
        }
        return bookDescriptions;
    }
}
