package by.albov.course.controller.book;

/**
 * Created by USER on 29.09.2014.
 */
public class EditBookRequest {
    public String title;
    public String description;
    public Long bookId;
    public Long genreId;
    public Long version;

    public EditBookRequest() {
    }

    public EditBookRequest(String title, String description, Long bookId, Long genreId) {
        this.title = title;
        this.description = description;
        this.bookId = bookId;
        this.genreId = genreId;
    }

    public EditBookRequest(String title, String description, Long bookId, Long genreId, Long version) {
        this.title = title;
        this.description = description;
        this.bookId = bookId;
        this.genreId = genreId;
        this.version = version;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Long getGenreId() {
        return genreId;
    }

    public void setGenreId(Long genreId) {
        this.genreId = genreId;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}
