package by.albov.course.controller.chapter;

import java.util.List;

/**
 * Created by USER on 06.10.2014.
 */
public class ChaptersSortRequest {
    private List<String> order;
    private Long bookVersion;

    public ChaptersSortRequest() {
    }

    public ChaptersSortRequest(List<String> order) {
        this.order = order;
    }

    public ChaptersSortRequest(List<String> order, Long bookVersion) {
        this.order = order;
        this.bookVersion = bookVersion;
    }

    public List<String> getOrder() {
        return order;
    }

    public void setOrder(List<String> order) {
        this.order = order;
    }

    public Long getBookVersion() {
        return bookVersion;
    }

    public void setBookVersion(Long bookVersion) {
        this.bookVersion = bookVersion;
    }
}
