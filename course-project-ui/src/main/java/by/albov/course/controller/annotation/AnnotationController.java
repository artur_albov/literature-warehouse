package by.albov.course.controller.annotation;

import by.albov.course.entity.Annotation;
import by.albov.course.service.annotation.AnnotationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by USER on 04.10.2014.
 */
@Controller
@RequestMapping("/chapters/{chapter_id}/annot")
public class AnnotationController {


    @Autowired
    private AnnotationService annotationService;

    @RequestMapping(value = "/{part}/get", method = RequestMethod.GET,consumes = MediaType.APPLICATION_JSON_VALUE) @ResponseBody
    public List<Annotation> getAllPartAnnotations(@PathVariable(value = "chapter_id") Long chapterId,
                                           @PathVariable(value = "part") Long part) {
        return annotationService.getChapterAnnotationByPart(chapterId,part);
    }

    @RequestMapping(value = "/{part}/add",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Annotation addAnnotation(@PathVariable(value = "chapter_id") Long chapterId,
                                    @PathVariable(value = "part") Long part,
                                    @RequestBody AnnotationRequest request){
        return annotationService.addNewAnnotation(chapterId,part,request.getContent());
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET,consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long deleteAnnotation(@PathVariable(value = "id")Long id) {
        annotationService.delete(annotationService.getById(id));
        return id;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Annotation getAnnotation(@PathVariable(value = "id")Long id){
        return annotationService.getById(id);
    }

    @RequestMapping(value = "/{id}/save", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Annotation saveAnnotation(@PathVariable(value = "id")Long id,@RequestBody AnnotationRequest request){
        return annotationService.rewriteAnnotation(id,request.getContent());
    }

}
