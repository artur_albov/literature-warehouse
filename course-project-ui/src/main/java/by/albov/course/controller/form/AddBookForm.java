package by.albov.course.controller.form;

import javax.validation.constraints.Size;

/**
 * Created by USER on 25.09.2014.
 */
public class AddBookForm {
    @Size(min = 1,max = 20,message = "Must be over 1 under 20 characters long!")
    private String title;
    private Long genreId;
    @Size(min = 1,max = 200,message = "Must be over 1 under 200 characters long!")
    private String description;

    public AddBookForm() {
    }

    public AddBookForm(String title, Long genreId) {
        this.title = title;
        this.genreId = genreId;
    }

    public AddBookForm(String title, Long genreId, String description) {
        this.title = title;
        this.genreId = genreId;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getGenreId() {
        return genreId;
    }

    public void setGenreId(Long genreId) {
        this.genreId = genreId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
