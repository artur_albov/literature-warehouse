package by.albov.course.controller.book;

import by.albov.course.entity.Chapter;
import by.albov.course.service.book.BookService;
import by.albov.course.service.chapter.ChapterService;
import org.markdown4j.Markdown4jProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by USER on 05.10.2014.
 */
@Controller
@RequestMapping("/show/{book_id}")
public class ShowBookController {

    @Autowired
    private BookService bookService;
    @Autowired
    private ChapterService chapterService;
    @Autowired
    private BookDescriptionLogic logic;

    @RequestMapping(value = "/{index}")
    public ModelAndView showChapter(@PathVariable(value = "book_id")Long bookId,@PathVariable(value = "index")Integer index,HttpServletResponse response,HttpServletRequest request) throws
            IOException {
        ModelAndView mav = new ModelAndView();
        Chapter chapter = chapterService.getChapterByBookIdAndIndex(bookId,index);
        if(chapter==null){
            mav.setViewName("redirect:/404");
            return mav;
        }
        Cookie chapterCookie = new Cookie(bookId.toString(), index.toString());
        chapterCookie.setMaxAge(600);
        chapterCookie.setPath("/");
        response.addCookie(chapterCookie);

        mav.addObject("description", logic.getBookDescription(bookService.getById(bookId)));
        mav.addObject("parts", chapterService.getArticlesList(bookId,index));
        mav.addObject("chapter", chapter);
        mav.addObject("chaptersSize",chapterService.getChapterByBookId(bookId).size());

        mav.setViewName("reader");
        return mav;
    }

    @RequestMapping(value = "")
    public ModelAndView showBook(@PathVariable(value = "book_id")Long bookId,HttpServletRequest request){
        ModelAndView mav = new ModelAndView();
        if(bookService.getById(bookId)==null){
            mav.setViewName("redirect:/404");
            return mav;
        }
        if(chapterService.getChapterByBookId(bookId).size()==0){
            mav.setViewName("redirect:/nocontent");
            return mav;
        }
        int chapterIndex = -1;
        Cookie[] cookies = request.getCookies();
        for(Cookie cookie: cookies){
            if(cookie.getName().equals(bookId.toString())){
                chapterIndex = Integer.parseInt(cookie.getValue());
                break;
            }
        }
        if(chapterIndex!=-1){
            mav.setViewName("redirect:/show/"+bookId+"/"+chapterIndex);
            return mav;
        }
        mav.setViewName("redirect:/show/"+bookId+"/0");
        return mav;
    }
}
