package by.albov.course.controller.book;

import by.albov.course.controller.form.AddBookForm;
import by.albov.course.entity.Book;
import by.albov.course.entity.User;
import by.albov.course.entity.common.UserRole;
import by.albov.course.service.Genre.GenreService;
import by.albov.course.service.book.BookService;
import by.albov.course.service.chapter.ChapterService;
import by.albov.course.service.rate.RateService;
import by.albov.course.service.security.CustomUserDetails;
import by.albov.course.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


import javax.validation.Valid;
import java.util.Date;

/**
 * Created by USER on 24.09.2014.
 */
@Controller
@RequestMapping("/books")
public class BookController {

    @Autowired
    private BookService bookService;
    @Autowired
    private ChapterService chapterService;
    @Autowired
    private GenreService genreService;
    @Autowired
    private BookDescriptionLogic logic;
    @Autowired
    private UserService userService;
    @Autowired
    private RateService rateService;

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public ModelAndView showAddPage() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("new-book");
        mav.addObject("genres", genreService.getAll());
        mav.addObject("book_form", new AddBookForm());
        mav.addObject("topBooks",rateService.getTopBooks(5,0));
        return mav;
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public ModelAndView addNewBook(@Valid @ModelAttribute("book_form") AddBookForm bookForm, BindingResult result) {
        ModelAndView mav = new ModelAndView();
        CustomUserDetails userDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!result.hasErrors()) {
            Book createdBook = bookService.saveAndFlush(
                    new Book(bookForm.getTitle(), bookForm.getDescription(), userDetails.getUser(),
                            genreService.getById(bookForm.getGenreId()), new Date()));
            mav.setViewName("redirect:/books/edit/" + createdBook.getId());
            return mav;
        }
        mav.addObject("genres",genreService.getAll());
        mav.setViewName("new-book");
        return mav;
    }

    @RequestMapping(value = "/edit/{id}")
    public ModelAndView showEditPage(@PathVariable(value = "id")Long bookId) {
        ModelAndView mav = new ModelAndView();
        Book book = bookService.getById(bookId);
        User loggedUser = userService.getLoggedUserDetails().getUser();
        if(book.getUser().getId().compareTo(loggedUser.getId()) != 0 && loggedUser.getUserRole() != UserRole.ROLE_ADMIN ){
            mav.setViewName("redirect:/403");
            return mav;
        }
        mav.setViewName("edit-book");
        mav.addObject("description",logic.getBookDescription(book));
        mav.addObject("genres",genreService.getAll());
        mav.addObject("chaptersCount",chapterService.getChapterCountByBookId(bookId));
        return mav;
    }



    @RequestMapping(value = "/save", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE) @ResponseBody
    public Long saveBookAjax(@RequestBody EditBookRequest request) throws Exception{
        Book book = bookService.updateBook(request.getTitle(),request.getDescription(),request.getGenreId(),request.getBookId(),request.getVersion());
        return book.getVersion();
    }

    @RequestMapping(value = "/my")
    public ModelAndView showUserBooks(){
        ModelAndView mav = new ModelAndView();
        CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        mav.addObject("books",bookService.getBooksByUserId(userDetails.getUser().getId()));
        mav.addObject("topBooks",rateService.getTopBooks(5,0));
        mav.setViewName("my-books");
        return mav;
    }

    @RequestMapping(value = "/delete/{id}")
    public ModelAndView deleteBook(@PathVariable(value = "id")Long bookId) {
        ModelAndView mav = new ModelAndView();
        Book book = bookService.getById(bookId);
        User loggedUser = userService.getLoggedUserDetails().getUser();
        if(book.getUser().getId().compareTo(loggedUser.getId()) != 0 && loggedUser.getUserRole() != UserRole.ROLE_ADMIN ){
            mav.setViewName("redirect:/403");
            return mav;
        }
        bookService.delete(book);
        mav.setViewName("redirect:/books/my");
        return mav;
    }



}
