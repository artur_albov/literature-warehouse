package by.albov.course.controller.tag;

/**
 * Created by USER on 05.10.2014.
 */
public class BookTagDescription {
    private Long id;
    private String tagName;

    public BookTagDescription() {
    }

    public BookTagDescription(Long id, String tagName) {
        this.id = id;
        this.tagName = tagName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
