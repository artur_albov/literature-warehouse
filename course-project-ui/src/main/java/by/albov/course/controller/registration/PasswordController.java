package by.albov.course.controller.registration;

import by.albov.course.controller.form.NewPasswordForm;
import by.albov.course.entity.User;
import by.albov.course.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

/**
 * Created by USER on 10.10.2014.
 */
@Controller
public class PasswordController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/{user_id}/password_reset",method = RequestMethod.GET)
    public ModelAndView showPasswordChangePage(@PathVariable(value = "user_id")Long userId){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("reset-password");
        mav.addObject("pass_form",new NewPasswordForm());
        return mav;
    }

    @RequestMapping(value = "/{user_id}/password_reset",method = RequestMethod.POST)
    public ModelAndView changePassword(@Valid @ModelAttribute(value = "pass_form")NewPasswordForm passwordForm,BindingResult result,@PathVariable(value = "user_id")Long userId){
        ModelAndView mav = new ModelAndView();
        User user = userService.getById(userId);
        if(!result.hasErrors()){
            if(passwordForm.getOldPassword().equals(user.getPassword())){
                if(passwordForm.getNewPassword().equals(passwordForm.getConfirmNewPassword())){
                    userService.changePassword(user.getId(),passwordForm.getNewPassword());
                    mav.setViewName("redirect:/");
                    return mav;
                }
            }
        }
        mav.setViewName("reset-password");
        return mav;
    }
}
