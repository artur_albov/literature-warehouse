package by.albov.course.controller.chapter;

import by.albov.course.entity.Book;
import by.albov.course.entity.Chapter;
import by.albov.course.service.book.BookService;
import by.albov.course.service.chapter.ChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by USER on 05.10.2014.
 */
@Controller
@RequestMapping("/chapters")
public class ChapterController {

    @Autowired
    private ChapterService chapterService;
    @Autowired
    private BookService bookService;


    @RequestMapping(value = "/new", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE) @ResponseBody
    public Chapter addNewChapter(@RequestBody ChapterRequest chapterRequest) throws Exception{
        Chapter createdChapter = chapterService.addNewChapter(chapterRequest.getName(),chapterRequest.getContent(),chapterRequest.getIndex(),chapterRequest.getBookId(),chapterRequest.getBookVersion());
        return createdChapter;
    }

    @RequestMapping(value = "/delete/{id}/{book_version}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long deleteChapter(@PathVariable(value = "id")Long chapterId,@PathVariable(value = "book_version")Long bookVersion,HttpServletResponse response) throws Exception{
        Chapter chapter = chapterService.getById(chapterId);
        chapterService.deleteChapter(chapter, bookVersion);
        Cookie cookie = new Cookie(chapter.getBookId().toString(),"0");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
        return chapterId;
    }

    @RequestMapping(value = "/edit/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Chapter getEditChapter(@PathVariable(value = "id")Long chapterId){
        return chapterService.getById(chapterId);
    }

    @RequestMapping(value = "/edit/{id}/{version}",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Chapter editChapter(@PathVariable(value = "id") Long chapterId,
                               @PathVariable(value = "version") Long chapterVersion,
                               @RequestBody ChapterRequest request) throws Exception{
        Chapter updatedChapter = chapterService.updateChapter(request.getName(), request.getContent(), chapterId, chapterVersion);
        return updatedChapter;
    }

    @RequestMapping(value = "{book_id}/sort",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Integer sortChapters(@RequestBody ChaptersSortRequest request,@PathVariable(value = "book_id")Long bookId) throws Exception{
        chapterService.sortChapters(request.getOrder(), request.getBookVersion(), bookId);
        return 0;
    }
}
