package by.albov.course.controller.tag;

/**
 * Created by USER on 29.09.2014.
 */
public class TagRequest {
    private String value;
    private Long bookId;

    public TagRequest() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }
}
