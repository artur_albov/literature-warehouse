package by.albov.course.controller.tag;

import by.albov.course.entity.Book;
import by.albov.course.entity.BookTag;
import by.albov.course.entity.Tag;
import by.albov.course.service.book.BookService;
import by.albov.course.service.booktag.BookTagService;
import by.albov.course.service.tag.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * Created by USER on 29.09.2014.
 */
@Controller
@RequestMapping("/booktag")
public class TagController {

    @Autowired
    private BookService bookService;

    @Autowired
    private TagService tagService;

    @Autowired
    private BookTagService bookTagService;

    @RequestMapping(value = "/add", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE) @ResponseBody
    public BookTagDescription addNewTag(@RequestBody TagRequest tagRequest) {
        BookTag bookTag =  bookTagService.addNewBookTag(tagRequest.getBookId(),tagRequest.getValue());
        return new BookTagDescription(bookTag.getId(),bookTag.getTag().getName());
    }

    @RequestMapping(value = "/delete/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Long deleteTag(@PathVariable(value = "id")Long bookTagId){
        bookTagService.delete(bookTagService.getById(bookTagId));
        return bookTagId;
    }

    @RequestMapping(value = "/getTags", method = RequestMethod.GET)
    @ResponseBody
    public List<Tag> getTags(@RequestParam String tagName){
        return tagService.getTags(tagName);
    }

}
