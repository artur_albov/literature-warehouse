package by.albov.course.controller.form;

import javax.validation.constraints.Size;

/**
 * Created by USER on 10.10.2014.
 */
public class NewPasswordForm {
    @Size(max = 15, min = 5,message = "Must be over 5 under 15 chars long")
    public String oldPassword;
    @Size(max = 15, min = 5,message = "Must be over 5 under 15 chars long")
    private String newPassword;
    @Size(max = 15, min = 5,message = "Must be over 5 under 15 chars long")
    private String confirmNewPassword;

    public NewPasswordForm() {
    }

    public NewPasswordForm(String oldPassword, String newPassword, String confirmNewPassword) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.confirmNewPassword = confirmNewPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }
}
