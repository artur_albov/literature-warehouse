package by.albov.course.controller.book;

import by.albov.course.entity.Book;
import by.albov.course.entity.BookTag;
import by.albov.course.entity.Chapter;
import by.albov.course.entity.Tag;

import java.util.List;

/**
 * Created by USER on 03.10.2014.
 */
public class BookDescription {
    private Book book;
    private Double rating;
    private int votes;
    private List<Chapter> chapters;
    private List<BookTag> tags;

    public BookDescription() {
    }

    public BookDescription(Book book, Double rating, int votes, List<Chapter> chapters) {
        this.book = book;
        this.rating = rating;
        this.votes = votes;
        this.chapters = chapters;
    }

    public BookDescription(Book book, Double rating, int votes, List<Chapter> chapters, List<BookTag> tags) {
        this.book = book;
        this.rating = rating;
        this.votes = votes;
        this.chapters = chapters;
        this.tags = tags;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public List<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(List<Chapter> chapters) {
        this.chapters = chapters;
    }

    public List<BookTag> getTags() {
        return tags;
    }

    public void setTags(List<BookTag> tags) {
        this.tags = tags;
    }
}
