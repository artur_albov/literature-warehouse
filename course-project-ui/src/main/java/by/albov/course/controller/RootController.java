package by.albov.course.controller;


import by.albov.course.controller.book.BookDescriptionLogic;
import by.albov.course.service.book.BookService;
import by.albov.course.service.rate.RateService;
import by.albov.course.service.tag.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
/**
 * Created by USER on 20.09.2014.
 */
@Controller
public class RootController {


    @Autowired
    private BookService bookService;

    @Autowired
    private BookDescriptionLogic logic;

    @Autowired
    private RateService rateService;

    @Autowired
    private TagService tagService;

    @RequestMapping("/")
    public ModelAndView showHomePage(@RequestParam(value = "error",required = false)boolean hasErrors){
        ModelAndView mav = new ModelAndView();
        if(hasErrors){
            mav.addObject("errorMessage","Account doesn't exists or locked!");
        } else {
            mav.addObject("errorMessage","");
        }
        mav.setViewName("home");
        return mav;
    }

    @RequestMapping("/lib/{page}")
    public ModelAndView showBooksPage(@PathVariable(value = "page")int page){
        ModelAndView mav = new ModelAndView();
        if(page < 0){
            mav.setViewName("redirect:/lib/0");
            return mav;
        }
        mav.addObject("page",bookService.getLastModified(new PageRequest(page,5)));
        mav.addObject("topBooks",rateService.getTopBooks(5, 0));
        mav.addObject("tags",tagService.getAll());
        mav.setViewName("books");
        return mav;
    }

    @RequestMapping("/404")
    public String pageNotFound(){
        return "404";
    }

    @RequestMapping("/403")
    public String accessDeniedPage(){
        return "403";
    }

    @RequestMapping("/nocontent")
    public String noContentPage(){
        return "no-content";
    }

}
