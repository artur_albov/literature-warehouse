package by.albov.course.controller.registration;

import by.albov.course.entity.User;
import by.albov.course.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by USER on 24.09.2014.
 */
@Controller
@RequestMapping("/registration/confirmation")
public class ConfirmationController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "")
    public ModelAndView showConfirmationPage(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("conf");
        return modelAndView;
    }

    @RequestMapping(value = "/success")
    public ModelAndView confirmUserAccount(@RequestParam(value = "confirm",required = true)String confimationCode){
        ModelAndView mav = new ModelAndView();
        User user = userService.getByConfirmationToken(confimationCode);
        user.setActive(true);
        userService.saveAndFlush(user);
        mav.addObject("user",user);
        mav.setViewName("conf-success");
        return mav;
    }



}
