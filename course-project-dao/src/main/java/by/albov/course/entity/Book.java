package by.albov.course.entity;

import by.albov.course.entity.common.AbstractEntity;
import org.hibernate.annotations.Type;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by USER on 20.09.2014.
 */
@Entity
@Indexed
@Table(name = Book.TABLE_NAME)
public class Book extends AbstractEntity {
    public static final String TABLE_NAME = "books";

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
    @Column(nullable = false)
    private String title;

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
    @Column(nullable = false)
    private String description;

    @ManyToOne
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "genre_id",referencedColumnName = "id")
    private Genre genre;

    @OneToMany(mappedBy = "bookId",cascade = CascadeType.ALL)
    private List<BookRate> bookRates;

    @IndexedEmbedded
    @OneToMany(mappedBy = "bookId",cascade = CascadeType.ALL)
    private List<Chapter> chapters;

    @IndexedEmbedded
    @OneToMany(mappedBy = "book")
    private List<BookTag> tags;

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
    @DateBridge(resolution = Resolution.DAY)
    @Column(nullable = false)
    @Type(type = "timestamp")
    private Date lastModified;

    public Book() {
    }

    public Book(String title, User user, Genre genre) {
        this.title = title;
        this.user = user;
        this.genre = genre;
    }

    public Book(String title, String description, User user, Genre genre, Date lastModified) {
        this.title = title;
        this.description = description;
        this.user = user;
        this.genre = genre;
        this.lastModified = lastModified;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<BookRate> getBookRates() {
        return bookRates;
    }

    public void setBookRates(List<BookRate> bookRates) {
        this.bookRates = bookRates;
    }

    public List<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(List<Chapter> chapters) {
        this.chapters = chapters;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
