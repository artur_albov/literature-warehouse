package by.albov.course.entity;

import by.albov.course.entity.common.AbstractEntity;
import by.albov.course.entity.common.UserRole;
import org.hibernate.search.annotations.Field;

import javax.persistence.*;
import java.util.List;

/**
 * Created by USER on 19.09.2014.
 */
@Entity
@Table(name = User.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {"email"}))
public class User extends AbstractEntity {
    public static final String TABLE_NAME = "users";

    @Column(nullable = false)
    private String email;
    @Column(nullable = false)
    private String password;
    @Field
    @Column(nullable = false)
    private String name;
    @Field
    @Column(nullable = false)
    private String surname;
    @Field
    @Column(nullable = false)
    private String nickname;
    @Column(nullable = false)
    private UserRole userRole;
    @Column(nullable = false)
    private boolean banned;
    @Column(nullable = false)
    private boolean active;
    @Column(nullable = false)
    private boolean passwordReserted;
    @Column
    private String confirmationToken;

    @OneToMany(mappedBy = "user")
    private List<Book> books;

    @OneToMany(mappedBy = "userId")
    private List<Annotation> annotations;

    public User() {
        this.banned = false;
        this.active = false;
        this.passwordReserted = false;
    }

    public User(String email, String password, String name, String surname, String nickname, UserRole userRole) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.nickname = nickname;
        this.userRole = userRole;
    }

    public User(String email, String password, String name, String surname, String nickname, UserRole userRole, boolean banned, boolean active, boolean passwordReserted) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.nickname = nickname;
        this.userRole = userRole;
        this.banned = banned;
        this.active = active;
        this.passwordReserted = passwordReserted;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isPasswordReserted() {
        return passwordReserted;
    }

    public void setPasswordReserted(boolean passwordReserted) {
        this.passwordReserted = passwordReserted;
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }

    public void setConfirmationToken(String confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }
}
