package by.albov.course.entity;

import by.albov.course.entity.common.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by USER on 20.09.2014.
 */
@Entity
@Table(name = Tag.TABLE_NAME)
public class Tag extends AbstractEntity {
    public static final String TABLE_NAME = "tags";

    @Column(nullable = false)
    private String name;

    @OneToMany(mappedBy = "tag")
    private List<BookTag> books;

    public Tag() {
    }

    public Tag(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
