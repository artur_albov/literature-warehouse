package by.albov.course.entity;

import by.albov.course.entity.common.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by USER on 20.09.2014.
 */
@Entity
@Table(name = Annotation.TABLE_NAME)
public class Annotation extends AbstractEntity {
    public static final String TABLE_NAME = "annotations";

    @Column(nullable = false)
    public Long userId;

    @Column(nullable = false)
    public Long chapterId;

    @Column(nullable = false)
    public String content;

    @Column(nullable = false)
    public Long partIndex;

    public Annotation() {
    }

    public Annotation(Long userId, Long chapterId, String content) {
        this.userId = userId;
        this.chapterId = chapterId;
        this.content = content;
    }

    public Annotation(Long userId, Long chapterId, String content, Long partIndex) {
        this.userId = userId;
        this.chapterId = chapterId;
        this.content = content;
        this.partIndex = partIndex;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getChapterId() {
        return chapterId;
    }

    public void setChapterId(Long chapterId) {
        this.chapterId = chapterId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getPartIndex() {
        return partIndex;
    }

    public void setPartIndex(Long partIndex) {
        this.partIndex = partIndex;
    }
}
