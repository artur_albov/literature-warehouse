package by.albov.course.entity.common;

/**
 * Created by USER on 01.10.2014.
 */
public enum Rate {
    ZERO(0), ONE(1), TWO(2), THREE(3), FOUR(4), FIVE(5);

    private int score;

    private Rate(int score) {
        this.score = score;
    }

    public static Rate getOfferRateByScore(int score) {
        for (Rate rate: Rate.values()) {
            if (rate.score == score) {
                return rate;
            }
        }
        return ZERO;
    }

}
