package by.albov.course.entity.common;

/**
 * Created by USER on 20.09.2014.
 */
public enum UserRole {
    ROLE_ADMIN, ROLE_USER, ROLE_ANONYMUS;

    public UserRole getRoleUser(){
        return UserRole.ROLE_USER;
    }
}
