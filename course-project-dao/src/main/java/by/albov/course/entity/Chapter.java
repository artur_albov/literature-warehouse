package by.albov.course.entity;

import by.albov.course.entity.common.AbstractIndexedEntity;
import org.hibernate.annotations.Type;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by USER on 20.09.2014.
 */
@Entity
@Table(name = Chapter.TABLE_NAME)
public class Chapter extends AbstractIndexedEntity {

    public static final String TABLE_NAME = "chapters";

    @Column(nullable = false)
    @Field
    private String name;

    @Column(nullable = false) @Field
    @Type(type = "text")
    private String content;

    @Column(nullable = false)
    private Integer index;

    @Column(nullable = false)
    private Long bookId;

    @OneToMany(mappedBy = "chapterId",cascade = CascadeType.ALL)
    private List<Annotation> annotations;


    public Chapter(String name, String content, Integer index, Long bookId) {
        this.name = name;
        this.content = content;
        this.index = index;
        this.bookId = bookId;
    }

    public Chapter() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }
}
