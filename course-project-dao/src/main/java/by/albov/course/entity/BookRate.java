package by.albov.course.entity;

import by.albov.course.entity.common.AbstractEntity;
import by.albov.course.entity.common.Rate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Created by USER on 20.09.2014.
 */
@Entity
@Table(name = BookRate.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames={"userId", "bookId"}))
public class BookRate extends AbstractEntity{
    public static final String TABLE_NAME = "rates";

    @Column(nullable = false)
    private Rate value;

    @Column(nullable = false)
    private Long userId;

    @Column(nullable = false)
    private Long bookId;

    public BookRate() {
    }

    public BookRate(Rate value, Long userId, Long bookId) {
        this.value = value;
        this.userId = userId;
        this.bookId = bookId;
    }

    public Rate getValue() {
        return value;
    }

    public void setValue(Rate value) {
        this.value = value;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }
}
