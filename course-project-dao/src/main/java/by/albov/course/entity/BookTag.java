package by.albov.course.entity;

import by.albov.course.entity.common.AbstractEntity;
import org.apache.solr.client.solrj.beans.Field;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by USER on 05.10.2014.
 */
@Entity
public class BookTag extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "book_id",referencedColumnName = "id")
    private Book book;

    @Field
    @ManyToOne
    @JoinColumn(name = "tag_id",referencedColumnName = "id")
    private Tag tag;

    public BookTag() {
    }

    public BookTag(Book book, Tag tag) {
        this.book = book;
        this.tag = tag;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
}
