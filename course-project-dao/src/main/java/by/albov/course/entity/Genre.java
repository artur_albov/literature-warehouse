package by.albov.course.entity;

import by.albov.course.entity.common.AbstractEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by USER on 20.09.2014.
 */
@Entity
@Table(name = Genre.TABLE_NAME)
public class Genre extends AbstractEntity {
    public static final String TABLE_NAME = "genres";

    @Column(nullable = false)
    private String name;

    @OneToMany(mappedBy = "genre")
    private List<Book> books;

    public Genre() {
    }

    public Genre(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
