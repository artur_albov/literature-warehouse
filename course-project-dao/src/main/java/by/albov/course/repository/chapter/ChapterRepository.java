package by.albov.course.repository.chapter;

import by.albov.course.entity.Book;
import by.albov.course.entity.Chapter;
import by.albov.course.repository.common.base.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by USER on 20.09.2014.
 */
@Repository
public interface ChapterRepository extends BaseRepository<Chapter>{

    @Query("select chapter from Chapter as chapter where chapter.bookId=:book_id order by chapter.index asc")
    public List<Chapter> getChaptersByBookId(@Param("book_id")Long bookId);

    @Query("select chapter from Chapter as chapter where chapter.bookId=:book_id and chapter.index=:chapter_index")
    public Chapter getChapterByBookIdAndIndex(@Param("book_id")Long bookId,@Param("chapter_index")Integer index);

    @Query("select count(c) from Chapter as c where c.bookId=:book_id")
    public int getChapterCountByBookId(@Param("book_id")Long bookId);

    @Query("select c from Chapter as  c where c.id=:chapter_id and c.version=:chapter_version")
    public Chapter getByIdAndVersion(@Param("chapter_id")Long chapterId,@Param("chapter_version")Long chapterVersion);
}
