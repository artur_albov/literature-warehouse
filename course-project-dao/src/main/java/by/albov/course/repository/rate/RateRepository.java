package by.albov.course.repository.rate;

import by.albov.course.entity.BookRate;
import by.albov.course.repository.common.base.BaseRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by USER on 20.09.2014.
 */
@Repository
public interface RateRepository extends BaseRepository<BookRate> {

    @Query("select avg(r.value) from BookRate as r where r.bookId=:book_id")
    public Double getCurrentBookRate(@Param(value = "book_id")Long bookId);

    @Query("select count(r) from BookRate as r where r.bookId=:book_id")
    public int getBookRateCount(@Param(value = "book_id")Long bookId);

    @Query("select r from BookRate as r where r.bookId=:book_id and r.userId=:user_id")
    public BookRate findRateByBookAndUserId(@Param(value = "book_id")Long bookId,@Param(value = "user_id")Long userId);

    @Query("select book,(select avg(r.value) from BookRate as r where r.bookId = book.id) as rating "
            +" from Book as book where (select avg(r.value) from BookRate as r where r.bookId = book.id) is not null order by rating desc")
    public List<Object[]> getTopBooks(Pageable pageable);

}
