package by.albov.course.repository.user;

import by.albov.course.entity.User;
import by.albov.course.repository.common.base.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by USER on 19.09.2014.
 */
@Repository
public interface UserRepository extends BaseRepository<User> {
    public User findByEmail(String email);
    public User findByConfirmationToken(String confirmationToken);
}
