package by.albov.course.repository.book;

import by.albov.course.entity.Book;
import by.albov.course.repository.common.base.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import java.util.List;

/**
 * Created by USER on 20.09.2014.
 */
@Repository
public interface BookRepository extends BaseRepository<Book> {

    @Query("select book from Book as book where book.user.id=:user_id")
    public List<Book> getBooksByUserId(@Param(value = "user_id")Long id);

    @Query("select book from Book as book order by book.lastModified desc")
    public Page<Book> getLastModifiedBooks(Pageable pageable);

    @Query("select book from Book as book where book.id=:book_id and book.version=:book_version")
    public Book getByIdAndVersion(@Param(value = "book_id")Long id,@Param(value = "book_version")Long version);
}
