package by.albov.course.repository.genre;

import by.albov.course.entity.Genre;
import by.albov.course.repository.common.base.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by USER on 20.09.2014.
 */
@Repository
public interface GenreRepository extends BaseRepository<Genre>{
}
