package by.albov.course.repository.tag;

import by.albov.course.entity.Tag;
import by.albov.course.repository.common.base.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by USER on 20.09.2014.
 */
@Repository
public interface TagRepository extends BaseRepository<Tag> {
    public Tag findByName(String name);

    @Query("select t from Tag as t where t.name like :tag_name")
    public List<Tag> getTags(@Param(value = "tag_name")String name);
}
