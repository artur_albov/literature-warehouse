package by.albov.course.repository.annotation;

import by.albov.course.entity.Annotation;
import by.albov.course.repository.common.base.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by USER on 20.09.2014.
 */
@Repository
public interface AnnotationRepository extends BaseRepository<Annotation> {

    @Query("select a from Annotation as a where a.chapterId=:chapter_id and a.userId=:user_id and a.partIndex=:part")
    public List<Annotation> getChapterAnnotationByPart(@Param(value = "chapter_id") Long chapterId,
                                                       @Param(value = "user_id") Long userId,
                                                       @Param(value = "part") Long part);

    @Query("select a from Annotation as a where a.chapterId=:chapter_id")
    public List<Annotation> getAnnotationsByChapterId(@Param(value = "chapter_id")Long chapterId);

    @Query("select a from Annotation as a where a.userId=:user_id")
    public List<Annotation> getAnnotationsByUserId(@Param(value = "user_id")Long userId);
}
