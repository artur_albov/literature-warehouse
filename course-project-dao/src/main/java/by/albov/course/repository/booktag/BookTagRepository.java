package by.albov.course.repository.booktag;


import by.albov.course.entity.BookTag;
import by.albov.course.repository.common.base.BaseRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by USER on 05.10.2014.
 */
@Repository
public interface BookTagRepository extends BaseRepository<BookTag> {

    @Query("select bt from BookTag as bt where bt.book.id=:book_id")
    public List<BookTag> findByBookId(@Param(value = "book_id")Long bookId);

    @Query("select bt from BookTag as bt where bt.book.id=:book_id and bt.tag.id=:tag_id")
    public BookTag findByBookIdAndTagId(@Param(value = "book_id")Long bookId,@Param(value = "tag_id")Long tagId);
}
