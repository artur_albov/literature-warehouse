package by.albov.course.service.user;

import by.albov.course.entity.User;
import by.albov.course.service.common.AbstractService;
import by.albov.course.service.security.CustomUserDetails;

/**
 * Created by USER on 19.09.2014.
 */
public interface UserService extends AbstractService<User> {
    public User getByEmail(String email);
    public User getByConfirmationToken(String confirmationToken);
    public CustomUserDetails getLoggedUserDetails();
    public void banUser(Long userId);
    public void unbanUser(Long userId);
    public void resetUserPassword(Long userId);
    public void changePassword(Long userId,String password);
}
