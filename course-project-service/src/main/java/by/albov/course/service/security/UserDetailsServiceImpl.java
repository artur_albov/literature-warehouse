package by.albov.course.service.security;

import by.albov.course.entity.User;
import by.albov.course.entity.common.UserRole;
import by.albov.course.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Администратор on 23.09.2014.
 */
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserDetails userDetails = null;
        try {
            User user = userService.getByEmail(s);
            userDetails = new CustomUserDetails(user.isActive(), true,
                    true, !user.isBanned(),
                    getAuthorities(user.getUserRole()), user);
        } catch (Exception e){
            throw new UsernameNotFoundException("Error in retrieving user");
        }
        return userDetails;
    }

    public Collection<GrantedAuthority> getAuthorities(UserRole userRole){
        List<GrantedAuthority> authorityList = new ArrayList<GrantedAuthority>(2);
        if(userRole == UserRole.ROLE_ADMIN){
            authorityList.add(new SimpleGrantedAuthority(UserRole.ROLE_ADMIN.name()));
        }
        authorityList.add(new SimpleGrantedAuthority(UserRole.ROLE_USER.name()));

        return authorityList;
    }
}
