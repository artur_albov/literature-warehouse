package by.albov.course.service.Genre;

import by.albov.course.entity.Genre;
import by.albov.course.repository.genre.GenreRepository;
import by.albov.course.service.common.AbstractServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by USER on 25.09.2014.
 */
@Service
public class GenreServiceImpl extends AbstractServiceImpl<Genre,GenreRepository> implements GenreService {

    @Autowired
    public GenreServiceImpl(GenreRepository repository) {
        super(repository);
    }
}
