package by.albov.course.service.rate;

import by.albov.course.entity.Book;
import by.albov.course.entity.BookRate;
import by.albov.course.repository.rate.RateRepository;
import by.albov.course.service.common.AbstractServiceImpl;
import by.albov.course.service.security.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER on 01.10.2014.
 */
@Service
public class RateServiceImpl extends AbstractServiceImpl<BookRate,RateRepository> implements RateService {

    @Autowired
    public RateServiceImpl(RateRepository repository) {
        super(repository);
    }

    @Override
    @Transactional
    public int getVotesCount(Long bookId) {
        return getRepository().getBookRateCount(bookId);
    }

    @Override
    @Transactional
    public Double getBookRate(Long bookId) {
        return getRepository().getCurrentBookRate(bookId);
    }

    @Override
    @Transactional
    public BookRate findRateByBookId(Long bookId) {
        CustomUserDetails userDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return getRepository().findRateByBookAndUserId(bookId,userDetails.getUser().getId());
    }

    @Override
    @Transactional
    public List<Book> getTopBooks(int count,int pageIndex) {
        Pageable pageable = new PageRequest(pageIndex,count);
        List<Book> books = new ArrayList<Book>();
        for(Object[] object: getRepository().getTopBooks(pageable)){
            books.add((Book)object[0]);
        }
        return books;
    }
}
