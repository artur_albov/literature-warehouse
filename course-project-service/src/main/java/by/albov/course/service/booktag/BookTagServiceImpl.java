package by.albov.course.service.booktag;

import by.albov.course.entity.Book;
import by.albov.course.entity.BookTag;
import by.albov.course.entity.Tag;
import by.albov.course.repository.booktag.BookTagRepository;
import by.albov.course.service.book.BookService;
import by.albov.course.service.common.AbstractServiceImpl;
import by.albov.course.service.tag.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by USER on 05.10.2014.
 */
@Service
public class BookTagServiceImpl extends AbstractServiceImpl<BookTag,BookTagRepository> implements BookTagService {


    @Autowired
    private BookService bookService;

    @Autowired
    private TagService tagService;

    @Autowired
    public BookTagServiceImpl(BookTagRepository repository) {
        super(repository);
    }

    @Override
    @Transactional
    public List<BookTag> findByBookId(Long bookId) {
        return getRepository().findByBookId(bookId);
    }

    @Override
    @Transactional
    public BookTag findByBookIdAndTagId(Long bookId, Long tagId) {
        return getRepository().findByBookIdAndTagId(bookId, tagId);
    }

    @Override
    @Transactional
    public BookTag addNewBookTag(Long bookId,String value) {
        Book book = bookService.getById(bookId);
        Tag tag = tagService.findByName(value);
        if(tag==null){
            tag = tagService.saveAndFlush(new Tag(value));
        }
        return saveAndFlush(new BookTag(book,tag));
    }
}
