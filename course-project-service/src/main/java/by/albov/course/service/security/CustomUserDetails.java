package by.albov.course.service.security;

import by.albov.course.entity.User;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by Администратор on 23.09.2014.
 */
public class CustomUserDetails extends org.springframework.security.core.userdetails.User {

    private User user;

    public CustomUserDetails(Collection<? extends GrantedAuthority> authorities, User user) {
        super(user.getEmail(), user.getPassword(), authorities);
        this.user = user;
    }

    public CustomUserDetails(boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities, User user) {
        super(user.getEmail(), user.getPassword(), enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
