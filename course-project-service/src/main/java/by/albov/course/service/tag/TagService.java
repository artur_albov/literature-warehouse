package by.albov.course.service.tag;

import by.albov.course.entity.Tag;
import by.albov.course.service.common.AbstractService;

import java.util.List;

/**
 * Created by USER on 29.09.2014.
 */
public interface TagService extends AbstractService<Tag> {
    public Tag findByName(String name);
    public List<Tag> getTags(String name);
}
