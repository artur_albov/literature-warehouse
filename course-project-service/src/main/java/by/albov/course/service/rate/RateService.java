package by.albov.course.service.rate;

import by.albov.course.entity.Book;
import by.albov.course.entity.BookRate;
import by.albov.course.service.common.AbstractService;
import org.springframework.data.domain.Page;

import java.awt.print.Pageable;
import java.util.List;

/**
 * Created by USER on 01.10.2014.
 */
public interface RateService extends AbstractService<BookRate>{
    public int getVotesCount(Long bookId);
    public Double getBookRate(Long bookId);
    public BookRate findRateByBookId(Long bookId);
    public List<Book> getTopBooks(int count,int pageIndex);
}
