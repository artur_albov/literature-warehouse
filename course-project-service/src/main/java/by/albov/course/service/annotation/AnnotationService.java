package by.albov.course.service.annotation;

import by.albov.course.entity.Annotation;
import by.albov.course.service.common.AbstractService;

import java.util.List;

/**
 * Created by USER on 04.10.2014.
 */
public interface AnnotationService extends AbstractService<Annotation> {
    public List<Annotation> getChapterAnnotationByPart(Long chapterId, Long part);
    public Annotation addNewAnnotation(Long chapterId,Long part,String content);
    public Annotation rewriteAnnotation(Long annotationId,String content);
    public List<Annotation> getAnnotationsBychapterId(Long chapterId);
    public List<Annotation> getAnnotationsByUserId(Long userId);
}
