package by.albov.course.service.book;

import by.albov.course.entity.Book;
import by.albov.course.service.common.AbstractService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by USER on 20.09.2014.
 */
public interface BookService extends AbstractService<Book> {
    public List<Book> getBooksByUserId(Long userId);
    public Page<Book> getLastModified(Pageable pageable);
    public Book getByIdAndVersion(Long id, Long version);
    public Book updateBook(String title,String description,Long genreId,Long id,Long version) throws Exception;
}
