package by.albov.course.service.common;

import by.albov.course.entity.common.AbstractEntity;
import by.albov.course.repository.common.base.BaseRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by USER on 19.09.2014.
 */
public class AbstractServiceImpl <E extends AbstractEntity,R extends BaseRepository<E>> implements AbstractService<E> {

    private R repository;

    public AbstractServiceImpl(R repository) {
        this.repository = repository;
    }

    @Override
    @Transactional
    public E getById(Long id) {
        return getRepository().findOne(id);
    }

    @Override
    @Transactional
    public E saveAndFlush(E entity) {
        return getRepository().saveAndFlush(entity);
    }

    @Override
    @Transactional
    public void delete(E entity) {
        getRepository().delete(entity);
        getRepository().flush();
    }

    @Override
    @Transactional
    public List<E> getAll() {
        return getRepository().findAll();
    }

    public R getRepository(){
        return repository;
    }
}
