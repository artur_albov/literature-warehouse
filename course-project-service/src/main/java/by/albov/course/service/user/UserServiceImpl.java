package by.albov.course.service.user;

import by.albov.course.entity.Annotation;
import by.albov.course.entity.User;
import by.albov.course.repository.user.UserRepository;
import by.albov.course.service.annotation.AnnotationService;
import by.albov.course.service.common.AbstractServiceImpl;
import by.albov.course.service.security.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by USER on 19.09.2014.
 */
@Service
public class UserServiceImpl extends AbstractServiceImpl<User,UserRepository> implements UserService {

    @Autowired
    private AnnotationService annotationService;

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        super(repository);
    }

    @Override
    @Transactional
    public User getByEmail(String email) {
        return getRepository().findByEmail(email);
    }

    @Override
    @Transactional
    public User getByConfirmationToken(String confirmationToken) {
        return getRepository().findByConfirmationToken(confirmationToken);
    }

    @Override
    public CustomUserDetails getLoggedUserDetails() {
        return (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    @Override
    @Transactional
    public void banUser(Long userId) {
        User user = getById(userId);
        user.setBanned(true);
    }

    @Override
    @Transactional
    public void unbanUser(Long userId) {
        User user = getById(userId);
        user.setBanned(false);
    }

    @Override
    @Transactional
    public void resetUserPassword(Long userId) {
        User user = getById(userId);
        user.setPasswordReserted(true);
    }

    @Override
    @Transactional
    public void changePassword(Long userId, String password) {
        User user = getById(userId);
        user.setPassword(password);
        user.setPasswordReserted(false);
    }

    @Override
    @Transactional
    public void delete(User entity) {
        List<Annotation> annotationList = annotationService.getAnnotationsByUserId(entity.getId());
        for(Annotation annotation: annotationList){
            annotationService.delete(annotation);
        }
        super.delete(entity);
    }
}
