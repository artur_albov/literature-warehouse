package by.albov.course.service.booktag;

import by.albov.course.entity.BookTag;
import by.albov.course.service.common.AbstractService;

import java.util.List;

/**
 * Created by USER on 05.10.2014.
 */
public interface BookTagService extends AbstractService<BookTag> {

    public List<BookTag> findByBookId(Long bookId);

    public BookTag findByBookIdAndTagId(Long bookId,Long tagId);

    public BookTag addNewBookTag(Long bookId,String value);
}
