package by.albov.course.service.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by USER on 10.10.2014.
 */
public class AuthHandler implements AuthenticationSuccessHandler {
    private AuthenticationSuccessHandler target = new SavedRequestAwareAuthenticationSuccessHandler();


    public boolean hasTemporaryPassword(Authentication authentication){
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        if(customUserDetails.getUser().isPasswordReserted())
            return true;
        else
            return false;
    }

    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response, Authentication auth){
        try{
            if ( hasTemporaryPassword(auth) ) {
                CustomUserDetails customUserDetails = (CustomUserDetails) auth.getPrincipal();
                response.sendRedirect("/"+customUserDetails.getUser().getId()+"/password_reset");
                SecurityContextHolder.clearContext();
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
            } else {
                response.sendRedirect("/lib/0");
                target.onAuthenticationSuccess(request, response, auth);
            }
        } catch (Exception e){

        }
    }

    public void proceed(HttpServletRequest request,
                        HttpServletResponse response, Authentication auth){
        try {
            target.onAuthenticationSuccess(request, response, auth);
        } catch (Exception e){

        }
    }
}
