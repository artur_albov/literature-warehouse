package by.albov.course.service.chapter;

import by.albov.course.entity.Annotation;
import by.albov.course.entity.Book;
import by.albov.course.entity.Chapter;
import by.albov.course.repository.chapter.ChapterRepository;
import by.albov.course.service.annotation.AnnotationService;
import by.albov.course.service.book.BookService;
import by.albov.course.service.common.AbstractServiceImpl;
import org.markdown4j.Markdown4jProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by USER on 20.09.2014.
 */
@Service
public class ChapterServiceImpl extends AbstractServiceImpl<Chapter,ChapterRepository> implements ChapterService {

    @Autowired
    private BookService bookService;

    @Autowired
    Markdown4jProcessor processor;

    @Autowired
    private AnnotationService annotationService;

    @Autowired
    public ChapterServiceImpl(ChapterRepository repository) {
        super(repository);
    }

    @Override
    @Transactional
    public List<Chapter> getChapterByBookId(Long bookId) {
        return getRepository().getChaptersByBookId(bookId);
    }

    @Override
    @Transactional
    public Chapter getChapterByBookIdAndIndex(Long bookId, Integer index) {
        return getRepository().getChapterByBookIdAndIndex(bookId, index);
    }

    @Override
    @Transactional
    public int getChapterCountByBookId(Long bookId) {
        return getRepository().getChapterCountByBookId(bookId);
    }

    @Override
    @Transactional
    public void delete(Chapter entity) {
        List<Chapter> chapters = getChapterByBookId(entity.getBookId());
        chapters.remove((int)entity.getIndex());
        for(int i = 0 ; i < chapters.size() ; i++){
            chapters.get(i).setIndex(i);
        }
        super.delete(entity);
    }

    @Override
    @Transactional
    public void deleteChapter(Chapter chapter, Long bookVersion) throws Exception{
        Book book = bookService.getByIdAndVersion(chapter.getBookId(),bookVersion);
        if(book==null)
            throw new Exception("Book versions not match!Please, update your page!");
        book.setLastModified(new Date());
        book.setVersion(book.getVersion()+1);
        delete(chapter);
    }

    @Override
    @Transactional
    public Chapter updateChapter(String name, String content, Long chapterId,Long chapterVersion) throws Exception{
        Chapter chapter = getRepository().getByIdAndVersion(chapterId,chapterVersion);
        if(chapter == null)
            throw new Exception("Chapter versions not match!");
        chapter.setContent(content);
        chapter.setName(name);
        List<Annotation> annotations = annotationService.getAnnotationsBychapterId(chapterId);
        for(Annotation a: annotations)
            annotationService.delete(a);
        Book book = bookService.getById(chapter.getBookId());
        book.setLastModified(new Date());
        return saveAndFlush(chapter);
    }

    @Override
    @Transactional
    public void sortChapters(List<String> order,Long bookVersion,Long bookId) throws Exception{
        Book book = bookService.getByIdAndVersion(bookId,bookVersion);
        if(book == null)
            throw new Exception("Book versions not match!");
        book.setLastModified(new Date());
        book.setVersion(book.getVersion()+1);
        List<String> idList = new ArrayList<String>();
        for(int i = 0 ; i < order.size() ; i++)
            idList.add(i,order.get(i).split("-")[1]);
        List<Chapter> chapters = new ArrayList<Chapter>();
        for(int i = 0 ; i < idList.size() ; i++)
            chapters.add(i,getById(Long.parseLong(idList.get(i))));
        for(int i = 0 ; i < chapters.size() ; i++)
            chapters.get(i).setIndex(i);
    }

    @Override
    @Transactional
    public Chapter addNewChapter(String name, String content, Integer index, Long bookId, Long bookVersion)
            throws Exception {
        Book book = bookService.getByIdAndVersion(bookId, bookVersion);
        if (book == null)
            throw new Exception("Book versions not match!");
        book.setLastModified(new Date());
        return saveAndFlush(new Chapter(name,content,index,bookId));
    }

    @Override
    @Transactional
    public String[] getArticlesList(Long bookId, Integer index) throws IOException{
        Chapter chapter = getChapterByBookIdAndIndex(bookId,index);
        String content = chapter.getContent();
        String[] parts = content.split("\n\n");
        for(int i = 0 ; i < parts.length ; i++){
            String temp = processor.process(parts[i]);
            parts[i] = temp;
            temp = parts[i].replace("<p>","");
            temp = temp.replace("</p>","");
            parts[i] = temp;
        }
        return parts;
    }
}
