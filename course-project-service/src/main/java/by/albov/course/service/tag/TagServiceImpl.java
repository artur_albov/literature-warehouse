package by.albov.course.service.tag;

import by.albov.course.entity.Tag;
import by.albov.course.repository.tag.TagRepository;
import by.albov.course.service.common.AbstractServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by USER on 29.09.2014.
 */
@Service
public class TagServiceImpl extends AbstractServiceImpl<Tag,TagRepository> implements TagService {

    @Autowired
    public TagServiceImpl(TagRepository repository) {
        super(repository);
    }

    @Override
    @Transactional
    public Tag findByName(String name) {
        return getRepository().findByName(name);
    }

    @Override
    @Transactional
    public List<Tag> getTags(String name) {
        String s = "%"+name+"%";
        return getRepository().getTags(s);
    }
}
