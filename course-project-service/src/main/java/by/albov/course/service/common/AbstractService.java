package by.albov.course.service.common;

import by.albov.course.entity.common.AbstractEntity;

import java.util.List;

/**
 * Created by USER on 19.09.2014.
 */
public interface AbstractService <E extends AbstractEntity> {
    public E getById(Long id);
    public E saveAndFlush(E entity);
    public void delete(E entity);
    public List<E> getAll();
}
