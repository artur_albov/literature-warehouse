package by.albov.course.service.book;

import by.albov.course.entity.Book;
import by.albov.course.entity.BookTag;
import by.albov.course.repository.book.BookRepository;
import by.albov.course.service.Genre.GenreService;
import by.albov.course.service.booktag.BookTagService;
import by.albov.course.service.common.AbstractServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



import java.util.Date;
import java.util.List;

/**
 * Created by USER on 20.09.2014.
 */
@Service
public class BookServiceImpl extends AbstractServiceImpl<Book,BookRepository> implements BookService {


    @Autowired
    private BookTagService bookTagService;

    @Autowired
    private GenreService genreService;


    @Autowired
    public BookServiceImpl(BookRepository repository) {
        super(repository);
    }

    @Override
    @Transactional
    public List<Book> getBooksByUserId(Long userId) {
        return getRepository().getBooksByUserId(userId);
    }

    @Override
    @Transactional
    public Page<Book> getLastModified(Pageable pageable) {
        return getRepository().getLastModifiedBooks(pageable);
    }

    @Override
    public void delete(Book entity) {
        List<BookTag> bookTags = bookTagService.findByBookId(entity.getId());
        for(BookTag bookTag: bookTags)
            bookTagService.delete(bookTag);
        super.delete(entity);
    }

    @Override
    @Transactional
    public Book getByIdAndVersion(Long id, Long version) {
        return getRepository().getByIdAndVersion(id,version);
    }

    @Override
    @Transactional
    public Book updateBook(String title, String description, Long genreId, Long id, Long version) throws Exception{
        Book book = getByIdAndVersion(id,version);
        if(book == null)
            throw new Exception("Book versions not match!");
        book.setTitle(title);
        book.setGenre(genreService.getById(genreId));
        book.setDescription(description);
        book.setLastModified(new Date());
        return book;
    }
}
