package by.albov.course.service.annotation;

import by.albov.course.entity.Annotation;
import by.albov.course.repository.annotation.AnnotationRepository;
import by.albov.course.service.common.AbstractServiceImpl;
import by.albov.course.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by USER on 04.10.2014.
 */
@Service
public class AnnotationServiceImpl extends AbstractServiceImpl<Annotation,AnnotationRepository> implements AnnotationService {

    @Autowired
    UserService userService;

    @Autowired
    public AnnotationServiceImpl(AnnotationRepository repository) {
        super(repository);
    }

    @Override
    @Transactional
    public List<Annotation> getChapterAnnotationByPart(Long chapterId, Long part) {
        return getRepository().getChapterAnnotationByPart(chapterId,userService.getLoggedUserDetails().getUser().getId(),part);
    }

    @Override
    @Transactional
    public Annotation addNewAnnotation(Long chapterId, Long part,String content) {
        Annotation annotation = new Annotation(userService.getLoggedUserDetails().getUser().getId(),chapterId,content,part);
        return saveAndFlush(annotation);
    }

    @Override
    @Transactional
    public Annotation rewriteAnnotation(Long annotationId, String content) {
        Annotation annotation = getById(annotationId);
        annotation.setContent(content);
        return saveAndFlush(annotation);
    }

    @Override
    @Transactional
    public List<Annotation> getAnnotationsBychapterId(Long chapterId) {
        return getRepository().getAnnotationsByChapterId(chapterId);
    }

    @Override
    @Transactional
    public List<Annotation> getAnnotationsByUserId(Long userId) {
        return getRepository().getAnnotationsByUserId(userId);
    }
}
