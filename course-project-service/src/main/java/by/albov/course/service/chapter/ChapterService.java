package by.albov.course.service.chapter;

import by.albov.course.entity.Annotation;
import by.albov.course.entity.Chapter;
import by.albov.course.service.common.AbstractService;

import java.io.IOException;
import java.util.List;

/**
 * Created by USER on 20.09.2014.
 */
public interface ChapterService extends AbstractService<Chapter> {
    public List<Chapter> getChapterByBookId(Long bookId);

    public Chapter getChapterByBookIdAndIndex(Long bookId,Integer index);

    public int getChapterCountByBookId(Long bookId);

    public Chapter updateChapter(String name,String content,Long chapterId,Long chapterVersion) throws Exception;

    public void sortChapters(List<String> order,Long bookVersion,Long bookId) throws Exception;

    public Chapter addNewChapter(String name,String content,Integer index,Long bookId,Long bookVersion) throws Exception;

    public void deleteChapter(Chapter chapter,Long bookVersion) throws Exception;

    public String[] getArticlesList(Long bookId,Integer index) throws IOException;


}
