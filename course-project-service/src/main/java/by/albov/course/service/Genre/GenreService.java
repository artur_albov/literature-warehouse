package by.albov.course.service.Genre;

import by.albov.course.entity.Genre;
import by.albov.course.service.common.AbstractService;

/**
 * Created by USER on 25.09.2014.
 */
public interface GenreService extends AbstractService<Genre> {
}
