package by.albov.course.service.book;

import by.albov.course.entity.Book;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

/**
 * Created by USER on 10.10.2014.
 */
@Service
public class BookSearchService {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    public void reindex(){
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManagerFactory.createEntityManager());
        try{
            fullTextEntityManager.createIndexer().startAndWait();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public List<Book> search(String word){
        EntityManager em = entityManagerFactory.createEntityManager();
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(em);

        em.getTransaction().begin();

        QueryBuilder qb = fullTextEntityManager.getSearchFactory()
                .buildQueryBuilder().forEntity(Book.class).get();
        org.apache.lucene.search.Query luceneQuery = qb
                .keyword()
                .onFields("title", "description", "chapters.name","chapters.content")
                .matching(word)
                .createQuery();

        // wrap Lucene query in a javax.persistence.Query
        javax.persistence.Query jpaQuery =
                fullTextEntityManager.createFullTextQuery(luceneQuery, Book.class);

        // execute search
        List result = jpaQuery.getResultList();

        em.getTransaction().commit();
        em.close();
        return result;
    }

}


