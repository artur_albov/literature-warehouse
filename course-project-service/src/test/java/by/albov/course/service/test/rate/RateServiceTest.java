package by.albov.course.service.test.rate;

import by.albov.course.entity.Book;
import by.albov.course.entity.BookRate;
import by.albov.course.entity.User;
import by.albov.course.entity.common.Rate;
import by.albov.course.service.book.BookService;
import by.albov.course.service.rate.RateService;
import by.albov.course.service.test.common.BaseServiceTest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

/**
 * @author albov  Date: 12.10.2014 Time: 23:16.
 */
public class RateServiceTest extends BaseServiceTest {
    @Autowired
    private BookService bookService;
    @Autowired
    private RateService rateService;

    @Rule public ExpectedException thrown= ExpectedException.none();

    @Test
    public void doubleRatingTest() {
        User user = getUser("testemail@email.com");
        Book book = getBook(user);
        BookRate bookRate = new BookRate(Rate.FIVE, user.getId(), book.getId());
        book.setBookRates(Collections.singletonList(bookRate));
        Book ratedBook = bookService.saveAndFlush(book);

        assertEquals(5, rateService.getBookRate(ratedBook.getId()),0.1);

        bookRate = new BookRate(Rate.ONE, user.getId(), book.getId());
        ratedBook.getBookRates().add(bookRate);
        thrown.expect(DataIntegrityViolationException.class);
        ratedBook = bookService.saveAndFlush(ratedBook);

        bookRepository.delete(book.getId());
        userRepository.delete(user.getId());
    }
}
