package by.albov.course.service.test.common;

import by.albov.course.entity.Book;
import by.albov.course.entity.User;
import by.albov.course.entity.common.UserRole;
import by.albov.course.repository.book.BookRepository;
import by.albov.course.repository.user.UserRepository;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

/**
 * @author albov  Date: 22.09.2014 Time: 16:19.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-context.xml")
public abstract class BaseServiceTest {

    @Autowired
    protected transient UserRepository userRepository;
    @Autowired
    protected transient BookRepository bookRepository;

    protected User getUser(String email) {
        User user = new User();
        user.setEmail(email);
        user.setName("name");
        user.setSurname("surname");
        user.setNickname("nickname");
        user.setPassword("password");
        user.setUserRole(UserRole.ROLE_ADMIN);
        user.setActive(true);
        user.setBanned(false);
        user.setPasswordReserted(false);
        return userRepository.save(user);
    }

    protected Book getBook(User user) {
        Book book = new Book();
        book.setTitle("title");
        book.setDescription("description");
        book.setUser(user);
        book.setLastModified(new Date());
        return bookRepository.save(book);
    }

}
